/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.additivecipherbruteforce;

/**
 *
 * @author Fowaz PC
 */
public class AdditiveCipher {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
                     
        //Brute Force
        for(int i=1; i<26; i++) {
            System.out.println(deCipher("CNK", i));
        }
    }
    
    public static String cipher(String s, int k) {
        char c;
        int index;
        String res = "";
        String alpha = "abcdefghijklmnopqrstuvwxyz";
        
        for(int i=0;i<s.length();i++) {
            index=alpha.indexOf(s.charAt(i));
            index=(index+k)%26;
            res+=alpha.charAt(index);
        }
        
        return res.toUpperCase();
    }
    
    public static String deCipher(String s, int k) {
        char c;
        int index;
        String res = "";
        String alpha = "abcdefghijklmnopqrstuvwxyz".toUpperCase();
        
        for(int i=0;i<s.length();i++) {
            index=alpha.indexOf(s.charAt(i));
            index=(index-k);
            index = index<0 ? index+26:index;
            res+=alpha.charAt(index);
        }
        
        return res.toLowerCase();
    }
    
}
