/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.additivecipherbruteforce;

import java.awt.Point;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Fowaz PC
 */
public class ADFGVXCipher {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Cipher("computer", "orange", "rinad");
    }
    
    public static char [] [] prepareKeyMat(String key) {
        char str[] = key.toLowerCase().toCharArray();
       // Create a set using String characters
        LinkedHashSet<Character> s = new LinkedHashSet();
      // HashSet doesn't allow repetition of elements
        for (char x : str)
            s.add(x);
        
        for (char x : "abcdefghijklmnopqrstuvwxyz0123456789".toCharArray())
            s.add(x);
 
        Character [] arr = new Character[36];
        arr=s.toArray(arr);
        
        char [] [] res = new char [6][6];
        
        for(int i=0; i<6; i++){
            for(int j=0; j<6; j++){
                res[i][j]=(char)arr[6*i+j];
            }
        }
        
        return res;
            
    }
    
    public static Point search(char [] [] arr, char c) {
        if(c=='j')
            c='i';
        
        for(int i=0; i<arr.length; i++)
            for(int j=0; j< arr[i].length; j++) {
                
                if(arr[i][j]==c){
                    return new Point(i, j);
                }
                
            }
        return new Point(-1, -1);
    }
    
    public static String getAlternativeText(char [] [] key, String s) {
        
        String res = "";
        
        for(char c : s.toCharArray()){
            Point p = search(key, c);
            res+=switchChar(p.x, p.y);
        }
        
        return res;
    }
    
    public static String switchChar(int row, int col) {
        
        String r="", c="";
        
        switch(row) {
            case 0: r="A"; break;
            case 1: r="D"; break;
            case 2: r="F"; break;
            case 3: r="G"; break;
            case 4: r="V"; break;
            case 5: r="X"; break;
        }
        
        switch(col) {
            case 0: c="A"; break;
            case 1: c="D"; break;
            case 2: c="F"; break;
            case 3: c="G"; break;
            case 4: c="V"; break;
            case 5: c="X"; break;
        }
        
        return r+c;
    }
    
    public static void Cipher (String text, String k1, String k2) {
        
        char [] [] key1  = prepareKeyMat(k1);
        String alternative = getAlternativeText(key1, text);
        String res="";
        
        char [] [] cipherMat=prepareCipherMatrix(alternative, k2);
        
        TreeMap<String, Integer> map = new TreeMap();
        
        for(int i=0; i<k2.length(); i++)
            map.put(Character.toString(k2.charAt(i)), i);
        
        for(Map.Entry<String, Integer> entry : map.entrySet())
            for(int j=0; j<cipherMat[entry.getValue()].length; j++)
                res+=cipherMat[entry.getValue()][j];
        
        System.out.println(res.toUpperCase());
        
    }
    
    public static char [] [] prepareCipherMatrix(String alternative, String k2) {
        int rows = k2.length();
        int cols = (int) Math.ceil((double)alternative.length()/k2.length());
        alternative = alternative.toUpperCase();
        
        while(alternative.length()<rows*cols)
            alternative+="A";
        
        char [] [] cipherMat = new char [rows][cols];
        for(int j=0; j<cols; j++)
            for(int i=0; i<rows; i++)
                cipherMat[i][j]=alternative.charAt(rows*j+i);
        
        return cipherMat;
        
        
    }
    
}
