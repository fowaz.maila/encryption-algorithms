/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.additivecipherbruteforce;

import java.util.Arrays;

/**
 *
 * @author Fowaz PC
 */
public class KnapSack {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int [] b = {2,3,6,13,27,52};
        int [] x = {0,1,1,0,0,0};
        int n=105;
        int r=31;
        
        int [] a = getPublic(b, n, r);
        System.out.println("a="+Arrays.toString(a));
        
        int s= knapSack(x, a);
        System.out.println("s="+s);
        
        int [] decipher = invKnapSack(s, b, n, r);
        System.out.println("x="+Arrays.toString(decipher));
        
    }
    
    public static int calcOpposit(int n, int k) {
          
        
        int r1=n, r2=k, t1=0, t2=1, q, r, t;
        
        while(r2>0) {
            q=r1/r2;
            r=r1-q*r2;
            t=t1-q*t2;
            
            r1=r2; r2=r;
            t1=t2; t2=t;
        }
        
        if(r1==1)
            return Math.floorMod(t1, n);
        else
            return Integer.MIN_VALUE;
    }
    
    public static int[] getPublic(int [] b, int n, int r){
        int [] res = new int [b.length];
        for(int i=0; i<b.length; i++){
            res[i]=(b[i]*r)%n;
        }
        return res;
    }
    
    public static int knapSack(int [] x, int [] a){
        int s=0;
        
        for(int i=0; i<x.length; i++)
            s+=x[i]*a[i];
        return s;
    }
    
    public static int [] invKnapSack(int s, int [] b, int n, int r){
        int [] res = new int [b.length];
        
        int r1= calcOpposit(n, r);
        int s1=(s*r1)%n;
        
        for(int i=b.length-1; i>=0; i--){
            if(s1>=b[i]){
                res[i]=1;
                s1=s1-b[i];
            }
        } 
        return res;
    }
    
    
    
}
