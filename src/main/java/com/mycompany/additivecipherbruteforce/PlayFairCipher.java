/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.additivecipherbruteforce;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;

/**
 *
 * @author Fowaz PC
 */
public class PlayFairCipher {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        System.out.println(cipher("hello", "lgdbaqmhecurnifxvsokzywtp"));
        System.out.println(deCipher("ECQZBX", "lgdbaqmhecurnifxvsokzywtp"));
        
        System.out.println(cipher("j", "lgdbaqmhecurnifxvsokzywtp"));
        System.out.println(deCipher("UO", "lgdbaqmhecurnifxvsokzywtp"));
    }
    
    public static String cipher(String s, String k) {
        
        char [] [] key = prepareKeyMat(k);
        s = separateSameLettersWithPadding(s);
        
        char x, y;
        Point px, py;
        int newR1, newR2, newC1, newC2;
        String res="";
        
        for(int i=0; i<s.length(); i+=2){
            x = s.charAt(i);
            y = s.charAt(i+1);
            px = search(key, x);
            py = search(key, y);
            
            if(px.x==py.x){
                newR1=px.x; newR2=py.x;
                newC1=Math.floorMod(px.y+1,5);
                newC2=Math.floorMod(py.y+1,5);
                
            } else if (px.y==py.y){
                newR1=Math.floorMod(px.x+1,5); newR2=Math.floorMod(py.x+1,5);
                newC1=px.y;
                newC2=py.y;
            } else {
                newR1=px.x; newR2=py.x;
                newC1=py.y;
                newC2=px.y;
            }
            
            res+=key[newR1][newC1];
            res+=key[newR2][newC2];
        }
        
        return res.toUpperCase();
    }
    
    public static String deCipher(String s, String k) {
        
        s=s.toLowerCase();
        char [] [] key = prepareKeyMat(k);
        
        char x, y;
        Point px, py;
        int newR1, newR2, newC1, newC2;
        String res="";
        
        for(int i=0; i<s.length(); i+=2){
            x = s.charAt(i);
            y = s.charAt(i+1);
            px = search(key, x);
            py = search(key, y);
            
            if(px.x==py.x){
                newR1=px.x; newR2=py.x;
                newC1=Math.floorMod(px.y-1,5);
                newC2=Math.floorMod(py.y-1,5);
                
            } else if (px.y==py.y){
                newR1=Math.floorMod(px.x-1,5); newR2=Math.floorMod(py.x-1,5);
                newC1=px.y;
                newC2=py.y;
            } else {
                newR1=px.x; newR2=py.x;
                newC1=py.y;
                newC2=px.y;
            }
            
            res+=key[newR1][newC1];
            res+=key[newR2][newC2];
        }
        
        return res.toLowerCase();
    }
    
    public static char [] [] prepareKeyMat(String key) {
        char str[] = key.toLowerCase().toCharArray();
       // Create a set using String characters
        LinkedHashSet<Character> s = new LinkedHashSet();
      // HashSet doesn't allow repetition of elements
        for (char x : str)
            s.add(x);
        
        for (char x : "abcdefghiklmnopqrstuvwxyz".toCharArray())
            s.add(x);
 
        Character [] arr = new Character[25];
        arr=s.toArray(arr);
        
        char [] [] res = new char [5][5];
        
        for(int i=0; i<5; i++){
            for(int j=0; j<5; j++){
                res[i][j]=(char)arr[5*i+j];
            }
        }
        
        return res;
            
    }
    
    public static String separateSameLettersWithPadding(String s) {
        String res="";
        
        for(int i=0; i<s.length()-1; i++){
            
            if(s.charAt(i)==s.charAt(i+1)){
                res+=s.charAt(i);
                res+='x';
            }
                
            else{
                res+=s.charAt(i);
            }
        }
        
        res+=s.charAt(s.length()-1);
        
        if(res.length()%2==1)
            res+='x';
        
        return res;
        
    }
    
    public static Point search(char [] [] arr, char c) {
        if(c=='j')
            c='i';
        
        for(int i=0; i<arr.length; i++)
            for(int j=0; j< arr[i].length; j++) {
                
                if(arr[i][j]==c){
                    return new Point(i, j);
                }
                
            }
        return new Point(-1, -1);
    }
}
