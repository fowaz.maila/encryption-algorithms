/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.additivecipherbruteforce;

/**
 *
 * @author Fowaz PC
 */
public class MultiplicativeCipher {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        String cipher = cipher("car", 5);
        System.out.println(cipher);
        
        String plain = deCipher(cipher, calcOpposit(26, 5));
        System.out.println(plain);
    }
    
    public static String cipher(String s, int k) {
        
        if(gcd(k,26)!=1)
            return "Can't Cipher! key must be prime with 26";
        
        char c;
        int index;
        String res = "";
        String alpha = "abcdefghijklmnopqrstuvwxyz";
        
        for(int i=0;i<s.length();i++) {
            index=alpha.indexOf(s.charAt(i));
            index=(index*k)%26;
            res+=alpha.charAt(index);
        }
        
        return res.toUpperCase();
    }
    
    public static String deCipher(String s, int k) {
        char c;
        int index;
        String res = "";
        String alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        
        for(int i=0;i<s.length();i++) {
            index=alpha.indexOf(s.charAt(i));
            index=(index*k)%26;
            res+=alpha.charAt(index);
        }
        
        return res.toLowerCase();
    }
    
    public static int gcd (int x, int y) {
        if (x==1 || y==1)
            return 1;
        
        while (x!=y) {
            if(x>y) {
                x=x-y;
            } else {
                y=y-x;
            }
            
        }
        
        return x;
    }
    
    public static int calcOpposit(int n, int k) {
          
        
        int r1=n, r2=k, t1=0, t2=1, q, r, t;
        
        while(r2>0) {
            q=r1/r2;
            r=r1-q*r2;
            t=t1-q*t2;
            
            r1=r2; r2=r;
            t1=t2; t2=t;
        }
        
        if(r1==1)
            return Math.floorMod(t1, 26);
        else
            return Integer.MIN_VALUE;
    }
    
}
