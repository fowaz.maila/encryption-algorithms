/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.additivecipherbruteforce;

import java.math.BigInteger;
import java.util.Arrays;

/**
 *
 * @author Fowaz PC
 */
public class DES {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        String s="0123456789ABCDEF";
        //s="8787878787878787";
        s=padStringWithZero(new BigInteger(s, 16).toString(2), 64);
        
        int [] block = new int [64];
        
        for(int i=0;i<s.length();i++)
            block[i]=Character.getNumericValue(s.charAt(i));
        
        String k="133457799BBCDFF1";
        //k="0E329232EA6D0D73";
        k = padStringWithZero(new BigInteger(k, 16).toString(2), 64);
        
        block=IP(block);
        block=cipherOneBlock(block, k);
        block=FP(block);
        
        s=arrToString(block);
        s=new BigInteger(s, 2).toString(16);
        System.out.println(s);
        
    }
    
    public static int[] IP(int [] arr) {
        arr=addArr0(arr);
        int [] res = new int [64];
        
        res[0]=arr[58];
        res[1]=arr[50];
        res[2]=arr[42];
        res[3]=arr[34];
        res[4]=arr[26];
        res[5]=arr[18];
        res[6]=arr[10];
        res[7]=arr[2];
        
        res[8]=arr[60];
        res[9]=arr[52];
        res[10]=arr[44];
        res[11]=arr[36];
        res[12]=arr[28];
        res[13]=arr[20];
        res[14]=arr[12];
        res[15]=arr[4];
        
        res[16]=arr[62];
        res[17]=arr[54];
        res[18]=arr[46];
        res[19]=arr[38];
        res[20]=arr[30];
        res[21]=arr[22];
        res[22]=arr[14];
        res[23]=arr[6];
        
        res[24]=arr[64];
        res[25]=arr[56];
        res[26]=arr[48];
        res[27]=arr[40];
        res[28]=arr[32];
        res[29]=arr[24];
        res[30]=arr[16];
        res[31]=arr[8];
        
        res[32]=arr[57];
        res[33]=arr[49];
        res[34]=arr[41];
        res[35]=arr[33];
        res[36]=arr[25];
        res[37]=arr[17];
        res[38]=arr[9];
        res[39]=arr[1];
        
        res[40]=arr[59];
        res[41]=arr[51];
        res[42]=arr[43];
        res[43]=arr[35];
        res[44]=arr[27];
        res[45]=arr[19];
        res[46]=arr[11];
        res[47]=arr[3];
        
        res[48]=arr[61];
        res[49]=arr[53];
        res[50]=arr[45];
        res[51]=arr[37];
        res[52]=arr[29];
        res[53]=arr[21];
        res[54]=arr[13];
        res[55]=arr[5];
        
        res[56]=arr[63];
        res[57]=arr[55];
        res[58]=arr[47];
        res[59]=arr[39];
        res[60]=arr[31];
        res[61]=arr[23];
        res[62]=arr[15];
        res[63]=arr[7];
        
        return res;
    }
    
    public static int[] FP(int [] arr) {
        arr=addArr0(arr);
        int [] res = new int [64];
        
        res[0]=arr[40];
        res[1]=arr[8];
        res[2]=arr[48];
        res[3]=arr[16];
        res[4]=arr[56];
        res[5]=arr[24];
        res[6]=arr[64];
        res[7]=arr[32];
        
        res[8]=arr[39];
        res[9]=arr[7];
        res[10]=arr[47];
        res[11]=arr[15];
        res[12]=arr[55];
        res[13]=arr[23];
        res[14]=arr[63];
        res[15]=arr[31];
        
        res[16]=arr[38];
        res[17]=arr[6];
        res[18]=arr[46];
        res[19]=arr[14];
        res[20]=arr[54];
        res[21]=arr[22];
        res[22]=arr[62];
        res[23]=arr[30];
        
        res[24]=arr[37];
        res[25]=arr[5];
        res[26]=arr[45];
        res[27]=arr[13];
        res[28]=arr[53];
        res[29]=arr[21];
        res[30]=arr[61];
        res[31]=arr[29];
        
        res[32]=arr[36];
        res[33]=arr[4];
        res[34]=arr[44];
        res[35]=arr[12];
        res[36]=arr[52];
        res[37]=arr[20];
        res[38]=arr[60];
        res[39]=arr[28];
        
        res[40]=arr[35];
        res[41]=arr[3];
        res[42]=arr[43];
        res[43]=arr[11];
        res[44]=arr[51];
        res[45]=arr[19];
        res[46]=arr[59];
        res[47]=arr[27];
        
        res[48]=arr[34];
        res[49]=arr[2];
        res[50]=arr[42];
        res[51]=arr[10];
        res[52]=arr[50];
        res[53]=arr[18];
        res[54]=arr[58];
        res[55]=arr[26];
        
        res[56]=arr[33];
        res[57]=arr[1];
        res[58]=arr[41];
        res[59]=arr[9];
        res[60]=arr[49];
        res[61]=arr[17];
        res[62]=arr[57];
        res[63]=arr[25];
        
        return res;
    }
    
    
    
    
    
    public static int [] cipherOneBlock(int [] block, String key) {
        //block is 64 bit block
        
        int [] L = new int [32], R=new int[32],Rcopy=new int[32], temp=new int[32], RExpand= new int [48], RxorK= new int [48];
        int [] s1,s2,s3,s4,s5,s6,s7,s8;
        String [] [] keys = generateKeys(key);
        int res [] = new int [64];
        
        //split block to L, R  32bit each
        for(int i=0; i<32; i++){
            L[i]=block[i];
            R[i]=block[i+32];
              
        }
        
        for(int i=1;i<=16;i++){
            Rcopy=R;
            RExpand = expandR(R);
            RxorK = RxorK(RExpand, convertHexStringToBinaryArray(keys[i-1][0]));
            s1=s1(new int [] {RxorK[0], RxorK[1], RxorK[2], RxorK[3], RxorK[4], RxorK[5] });
            s2=s2(new int [] {RxorK[6], RxorK[7], RxorK[8], RxorK[9], RxorK[10], RxorK[11] });
            s3=s3(new int [] {RxorK[12], RxorK[13], RxorK[14], RxorK[15], RxorK[16], RxorK[17] });
            s4=s4(new int [] {RxorK[18], RxorK[19], RxorK[20], RxorK[21], RxorK[22], RxorK[23] });
            s5=s5(new int [] {RxorK[24], RxorK[25], RxorK[26], RxorK[27], RxorK[28], RxorK[29] });
            s6=s6(new int [] {RxorK[30], RxorK[31], RxorK[32], RxorK[33], RxorK[34], RxorK[35] });
            s7=s7(new int [] {RxorK[36], RxorK[37], RxorK[38], RxorK[39], RxorK[40], RxorK[41] });
            s8=s8(new int [] {RxorK[42], RxorK[43], RxorK[44], RxorK[45], RxorK[46], RxorK[47] });
            
            temp = compineS(s1, s2, s3, s4, s5, s6, s7, s8);
            temp=P(temp);
            
            R=xor32bit(temp, L);
            L=Rcopy;
            
        }
        
        for(int i=0;i<L.length; i++){
            res[i]=R[i];
            res[i+32]=L[i];
        }
        
        return res;
    }
    
    public static int[] expandR(int [] R) {
        
        int [] res = new int[48];
        R=addArr0(R);
        
        res[0]=R[32];
        res[1]=R[1];
        res[2]=R[2];
        res[3]=R[3];
        res[4]=R[4];
        res[5]=R[5];
        
        res[6]=R[4];
        res[7]=R[5];
        res[8]=R[6];
        res[9]=R[7];
        res[10]=R[8];
        res[11]=R[9];
        
        res[12]=R[8];
        res[13]=R[9];
        res[14]=R[10];
        res[15]=R[11];
        res[16]=R[12];
        res[17]=R[13];
        
        res[18]=R[12];
        res[19]=R[13];
        res[20]=R[14];
        res[21]=R[15];
        res[22]=R[16];
        res[23]=R[17];
        
        res[24]=R[16];
        res[25]=R[17];
        res[26]=R[18];
        res[27]=R[19];
        res[28]=R[20];
        res[29]=R[21];
        
        res[30]=R[20];
        res[31]=R[21];
        res[32]=R[22];
        res[33]=R[23];
        res[34]=R[24];
        res[35]=R[25];
        
        res[36]=R[24];
        res[37]=R[25];
        res[38]=R[26];
        res[39]=R[27];
        res[40]=R[28];
        res[41]=R[29];
        
        res[42]=R[28];
        res[43]=R[29];
        res[44]=R[30];
        res[45]=R[31];
        res[46]=R[32];
        res[47]=R[1];
        
        return res;

    }
    
    public static int [] addArr0 (int [] arr) {
        int [] res = new int [arr.length+1];
        res[0]=-1;
        
        for(int i=0; i<arr.length; i++)
            res[i+1]=arr[i];
        
        return res;
    }
    
    public static int [] RxorK(int [] R, int [] k) {
        int [] res = new int [48];
        
        for(int i=0; i<res.length; i++)
            res[i]=R[i]^k[i];
        
        return res;
    }
    
    public static int [] xor32bit(int [] R, int [] k) {
        int [] res = new int [32];
        
        for(int i=0; i<res.length; i++)
            res[i]=R[i]^k[i];
        
        return res;
    }
    
    public static int [] s1 (int [] x){
        int row = Integer.parseInt(new BigInteger(x[0]+""+x[5],2).toString());
        int cols = Integer.parseInt(new BigInteger(x[1]+""+x[2]+""+x[3]+""+x[4],2).toString());
        int [] bin = new int [4];
        
        int [] [] s = {{14,4,13,1,2,15,11,8,3,10,6,12,5,9,0,7},
                       {0,15,7,4,14,2,13,1,10,6,12,11,9,5,3,8},
                       {4,1,14,8,13,6,2,11,15,12,9,7,3,10,5,0},
                       {15,12,8,2,4,9,1,7,5,11,3,14,10,0,6,13}};
        int res = s[row][cols];
        String r = new BigInteger(Integer.toString(res)).toString(2);
        
        for(int i=0; i<r.length(); i++)
            bin[4-r.length()+i]=Character.getNumericValue(r.charAt(i));
        
        return bin;
    }
    
    public static int [] s2 (int [] x){
        int row = Integer.parseInt(new BigInteger(x[0]+""+x[5],2).toString());
        int cols = Integer.parseInt(new BigInteger(x[1]+""+x[2]+""+x[3]+""+x[4],2).toString());
        int [] bin = new int [4];
        
        int [] [] s = {{15,1,8,14,6,11,3,4,9,7,2,13,12,0,5,10},
                       {3,13,4,7,15,2,8,14,12,0,1,10,6,9,11,5},
                       {0,14,7,11,10,4,13,1,5,8,12,6,9,3,2,15},
                       {13,8,10,1,3,15,4,2,11,6,7,12,0,5,14,9}};
        int res = s[row][cols];
        String r = new BigInteger(Integer.toString(res)).toString(2);
        
        for(int i=0; i<r.length(); i++)
            bin[4-r.length()+i]=Character.getNumericValue(r.charAt(i));
        
        return bin;
    }
    
    public static int [] s3 (int [] x){
        int row = Integer.parseInt(new BigInteger(x[0]+""+x[5],2).toString());
        int cols = Integer.parseInt(new BigInteger(x[1]+""+x[2]+""+x[3]+""+x[4],2).toString());
        int [] bin = new int [4];
        
        int [] [] s = {{10,0,9,14,6,3,15,5,1,13,12,7,11,4,2,8},
                       {13,7,0,9,3,4,6,10,2,8,5,14,12,11,15,1},
                       {13,6,4,9,8,15,3,0,11,1,2,12,5,10,14,7},
                       {1,10,13,0,6,9,8,7,4,15,14,3,11,5,2,12}};
        int res = s[row][cols];
        String r = new BigInteger(Integer.toString(res)).toString(2);
        
        for(int i=0; i<r.length(); i++)
            bin[4-r.length()+i]=Character.getNumericValue(r.charAt(i));
        
        return bin;
    }
    
    public static int [] s4 (int [] x){
        int row = Integer.parseInt(new BigInteger(x[0]+""+x[5],2).toString());
        int cols = Integer.parseInt(new BigInteger(x[1]+""+x[2]+""+x[3]+""+x[4],2).toString());
        int [] bin = new int [4];
        
        int [] [] s = {{7,13,14,3,0,6,9,10,1,2,8,5,11,12,4,15},
                       {13,8,11,5,6,15,0,3,4,7,2,12,1,10,14,9},
                       {10,6,9,0,12,11,7,13,15,1,3,14,5,2,8,4},
                       {3,15,0,6,10,1,13,8,9,4,5,11,12,7,2,14}};
        int res = s[row][cols];
        String r = new BigInteger(Integer.toString(res)).toString(2);
        
        for(int i=0; i<r.length(); i++)
            bin[4-r.length()+i]=Character.getNumericValue(r.charAt(i));
        
        return bin;
    }
    
    public static int [] s5 (int [] x){
        int row = Integer.parseInt(new BigInteger(x[0]+""+x[5],2).toString());
        int cols = Integer.parseInt(new BigInteger(x[1]+""+x[2]+""+x[3]+""+x[4],2).toString());
        int [] bin = new int [4];
        
        int [] [] s = {{2,12,4,1,7,10,11,6,8,5,3,15,13,0,14,9},
                       {14,11,2,12,4,7,13,1,5,0,15,10,3,9,8,6},
                       {4,2,1,11,10,13,7,8,15,9,12,5,6,3,0,14},
                       {11,8,12,7,1,14,2,13,6,15,0,9,10,4,5,3}};
        int res = s[row][cols];
        String r = new BigInteger(Integer.toString(res)).toString(2);
        
        for(int i=0; i<r.length(); i++)
            bin[4-r.length()+i]=Character.getNumericValue(r.charAt(i));
        
        return bin;
    }
    
    public static int [] s6 (int [] x){
        int row = Integer.parseInt(new BigInteger(x[0]+""+x[5],2).toString());
        int cols = Integer.parseInt(new BigInteger(x[1]+""+x[2]+""+x[3]+""+x[4],2).toString());
        int [] bin = new int [4];
        
        int [] [] s = {{12,1,10,15,9,2,6,8,0,13,3,4,14,7,5,11},
                       {10,15,4,2,7,12,9,5,6,1,13,14,0,11,3,8},
                       {9,14,15,5,2,8,12,3,7,0,4,10,1,13,11,6},
                       {4,3,2,12,9,5,15,10,11,14,1,7,6,0,8,13}};
        int res = s[row][cols];
        String r = new BigInteger(Integer.toString(res)).toString(2);
        
        for(int i=0; i<r.length(); i++)
            bin[4-r.length()+i]=Character.getNumericValue(r.charAt(i));
        
        return bin;
    }
    
    public static int [] s7 (int [] x){
        int row = Integer.parseInt(new BigInteger(x[0]+""+x[5],2).toString());
        int cols = Integer.parseInt(new BigInteger(x[1]+""+x[2]+""+x[3]+""+x[4],2).toString());
        int [] bin = new int [4];
        
        int [] [] s = {{4,11,2,14,15,0,8,13,3,12,9,7,5,10,6,1},
                       {13,0,11,7,4,9,1,10,14,3,5,12,2,15,8,6},
                       {1,4,11,13,12,3,7,14,10,15,6,8,0,5,9,2},
                       {6,11,13,8,1,4,10,7,9,5,0,15,14,2,3,12}};
        int res = s[row][cols];
        String r = new BigInteger(Integer.toString(res)).toString(2);
        
        for(int i=0; i<r.length(); i++)
            bin[4-r.length()+i]=Character.getNumericValue(r.charAt(i));
        
        return bin;
    }
    
    public static int [] s8 (int [] x){
        int row = Integer.parseInt(new BigInteger(x[0]+""+x[5],2).toString());
        int cols = Integer.parseInt(new BigInteger(x[1]+""+x[2]+""+x[3]+""+x[4],2).toString());
        int [] bin = new int [4];
        
        int [] [] s = {{13,2,8,4,6,15,11,1,10,9,3,14,5,0,12,7},
                       {1,15,13,8,10,3,7,4,12,5,6,11,0,14,9,2},
                       {7,11,4,1,9,12,14,2,0,6,10,13,15,3,5,8},
                       {2,1,14,7,4,10,8,13,15,12,9,0,3,5,6,11}};
        int res = s[row][cols];
        String r = new BigInteger(Integer.toString(res)).toString(2);
        
        for(int i=0; i<r.length(); i++)
            bin[4-r.length()+i]=Character.getNumericValue(r.charAt(i));
        
        return bin;
    }
    
    public static int [] compineS(int [] s1, int [] s2, int [] s3, int [] s4, int [] s5, int [] s6, int [] s7, int [] s8){
        int [] res = new int [32];
        
        for(int i=0; i<s1.length;i++)
            res[i]=s1[i];
        
        for(int i=0; i<s2.length;i++)
            res[i+4]=s2[i];
        
        for(int i=0; i<s3.length;i++)
            res[i+8]=s3[i];
        
        for(int i=0; i<s4.length;i++)
            res[i+12]=s4[i];
        
        for(int i=0; i<s5.length;i++)
            res[i+16]=s5[i];
        
        for(int i=0; i<s6.length;i++)
            res[i+20]=s6[i];
        
        for(int i=0; i<s7.length;i++)
            res[i+24]=s7[i];
        
        for(int i=0; i<s8.length;i++)
            res[i+28]=s8[i];
        
        return res;
    }
    
    public static int [] P(int [] arr){
        int [] res = new int[32];
        arr=addArr0(arr);
        
        res[0]=arr[16];
        res[1]=arr[7];
        res[2]=arr[20];
        res[3]=arr[21];
        res[4]=arr[29];
        res[5]=arr[12];
        res[6]=arr[28];
        res[7]=arr[17];
        res[8]=arr[1];
        res[9]=arr[15];
        res[10]=arr[23];
        res[11]=arr[26];
        res[12]=arr[5];
        res[13]=arr[18];
        res[14]=arr[31];
        res[15]=arr[10];
        res[16]=arr[2];
        res[17]=arr[8];
        res[18]=arr[24];
        res[19]=arr[14];
        res[20]=arr[32];
        res[21]=arr[27];
        res[22]=arr[3];
        res[23]=arr[9];
        res[24]=arr[19];
        res[25]=arr[13];
        res[26]=arr[30];
        res[27]=arr[6];
        res[28]=arr[22];
        res[29]=arr[11];
        res[30]=arr[4];
        res[31]=arr[25];
        
        return res;
    }
    
    
    
    
    public static String [] [] generateKeys(String s) {
        //s is assumed to be binary string
        
        String [] [] keys= new String [16][1];
        
        int [] k56 = generateKey56(s);
        int [] L = new int [28], R = new int [28];
        int [] temp = new int [56];
        int [] k48 = new int [48];
        
        //split
        for(int i=0; i<28; i++)
            L[i]=k56[i];
        for(int i=28; i<56; i++)
            R[i-28]=k56[i];
        
        for(int i=1; i<=16; i++){
            if(i==1 || i==2 || i==9 || i==16){
                L=shift(L, 1);
                R=shift(R, 1);
            } else {
                L=shift(L, 2);
                R=shift(R, 2);
            }
            
            temp = compine(L, R);
            k48=generateKey48(temp);
            String binaryStr = arrToString(k48);
            
            String hexStr = new BigInteger(binaryStr, 2).toString(16);
            
            keys[i-1][0]=hexStr;
            
        }
        
        return keys;
        
    }
    
    public static int [] compine(int [] L, int [] R){
        int [] t = new int [56];
        
        for(int i=0;i<L.length; i++){
            t[i]=L[i];
            t[i+28]=R[i];
        }
            
        return t;
    }
    
    public static int [] generateKey56(String s){
        //s is binary string with 64 bit
        //we will add 1 charcter to it to comply with the table
        s="-"+s;
        int [] k56 = new int[56];
        
        k56[0]=Character.getNumericValue(s.charAt(57));
        k56[1]=Character.getNumericValue(s.charAt(49));
        k56[2]=Character.getNumericValue(s.charAt(41));
        k56[3]=Character.getNumericValue(s.charAt(33));
        k56[4]=Character.getNumericValue(s.charAt(25));
        k56[5]=Character.getNumericValue(s.charAt(17));
        k56[6]=Character.getNumericValue(s.charAt(9));
        k56[7]=Character.getNumericValue(s.charAt(1));
        
        k56[8]=Character.getNumericValue(s.charAt(58));
        k56[9]=Character.getNumericValue(s.charAt(50));
        k56[10]=Character.getNumericValue(s.charAt(42));
        k56[11]=Character.getNumericValue(s.charAt(34));
        k56[12]=Character.getNumericValue(s.charAt(26));
        k56[13]=Character.getNumericValue(s.charAt(18));
        k56[14]=Character.getNumericValue(s.charAt(10));
        k56[15]=Character.getNumericValue(s.charAt(2));
        
        k56[16]=Character.getNumericValue(s.charAt(59));
        k56[17]=Character.getNumericValue(s.charAt(51));
        k56[18]=Character.getNumericValue(s.charAt(43));
        k56[19]=Character.getNumericValue(s.charAt(35));
        k56[20]=Character.getNumericValue(s.charAt(27));
        k56[21]=Character.getNumericValue(s.charAt(19));
        k56[22]=Character.getNumericValue(s.charAt(11));
        k56[23]=Character.getNumericValue(s.charAt(3));
        
        k56[24]=Character.getNumericValue(s.charAt(60));
        k56[25]=Character.getNumericValue(s.charAt(52));
        k56[26]=Character.getNumericValue(s.charAt(44));
        k56[27]=Character.getNumericValue(s.charAt(36));
        k56[28]=Character.getNumericValue(s.charAt(63));
        k56[29]=Character.getNumericValue(s.charAt(55));
        k56[30]=Character.getNumericValue(s.charAt(47));
        k56[31]=Character.getNumericValue(s.charAt(39));
        
        k56[32]=Character.getNumericValue(s.charAt(31));
        k56[33]=Character.getNumericValue(s.charAt(23));
        k56[34]=Character.getNumericValue(s.charAt(15));
        k56[35]=Character.getNumericValue(s.charAt(7));
        k56[36]=Character.getNumericValue(s.charAt(62));
        k56[37]=Character.getNumericValue(s.charAt(54));
        k56[38]=Character.getNumericValue(s.charAt(46));
        k56[39]=Character.getNumericValue(s.charAt(38));
        
        k56[40]=Character.getNumericValue(s.charAt(30));
        k56[41]=Character.getNumericValue(s.charAt(22));
        k56[42]=Character.getNumericValue(s.charAt(14));
        k56[43]=Character.getNumericValue(s.charAt(6));
        k56[44]=Character.getNumericValue(s.charAt(61));
        k56[45]=Character.getNumericValue(s.charAt(53));
        k56[46]=Character.getNumericValue(s.charAt(45));
        k56[47]=Character.getNumericValue(s.charAt(37));
        
        k56[48]=Character.getNumericValue(s.charAt(29));
        k56[49]=Character.getNumericValue(s.charAt(21));
        k56[50]=Character.getNumericValue(s.charAt(13));
        k56[51]=Character.getNumericValue(s.charAt(5));
        k56[52]=Character.getNumericValue(s.charAt(28));
        k56[53]=Character.getNumericValue(s.charAt(20));
        k56[54]=Character.getNumericValue(s.charAt(12));
        k56[55]=Character.getNumericValue(s.charAt(4));
        
        return k56;
        
    }
    
    public static int [] generateKey48(int [] k56){
        
        //necessary to comply with table values
        int [] temp = new int [57];
        for(int i=1; i<temp.length; i++){
            temp[i]=k56[i-1];
        }
        
        int [] k48 = new int [48];
        
        k48[0]=temp[14];
        k48[1]=temp[17];
        k48[2]=temp[11];
        k48[3]=temp[24];
        k48[4]=temp[1];
        k48[5]=temp[5];
        k48[6]=temp[3];
        k48[7]=temp[28];
        
        k48[8]=temp[15];
        k48[9]=temp[6];
        k48[10]=temp[21];
        k48[11]=temp[10];
        k48[12]=temp[23];
        k48[13]=temp[19];
        k48[14]=temp[12];
        k48[15]=temp[4];
        
        k48[16]=temp[26];
        k48[17]=temp[8];
        k48[18]=temp[16];
        k48[19]=temp[7];
        k48[20]=temp[27];
        k48[21]=temp[20];
        k48[22]=temp[13];
        k48[23]=temp[2];
        
        k48[24]=temp[41];
        k48[25]=temp[52];
        k48[26]=temp[31];
        k48[27]=temp[37];
        k48[28]=temp[47];
        k48[29]=temp[55];
        k48[30]=temp[30];
        k48[31]=temp[40];
        
        k48[32]=temp[51];
        k48[33]=temp[45];
        k48[34]=temp[33];
        k48[35]=temp[48];
        k48[36]=temp[44];
        k48[37]=temp[49];
        k48[38]=temp[39];
        k48[39]=temp[56];
        
        k48[40]=temp[34];
        k48[41]=temp[53];
        k48[42]=temp[46];
        k48[43]=temp[42];
        k48[44]=temp[50];
        k48[45]=temp[36];
        k48[46]=temp[29];
        k48[47]=temp[32];
        
        return k48;
    }
    
    public static int [] shift (int [] m, int d) {
        
        int [] res = new int [28];
        
        if(d==1){
            for(int i=0; i<m.length-1; i++){
                res[i]=m[i+1];
            }
            res[res.length-1]=m[0];
            
        } else {
            
            for(int i=0; i<m.length-2; i++){
                res[i]=m[i+2];
            }
            res[res.length-2]=m[0];
            res[res.length-1]=m[1];
            
        }
        
        return res;
    }
    
    
    
    public static String arrToString(int [] arr) {
        String res="";
        for(int x : arr)
            res+=x;
        return res;
    }
    
    public static String padStringWithZero(String s, int length){
        while(s.length()!=length)
            s="0"+s;
        return s;
    }
    
    public static int [] convertHexStringToBinaryArray(String hex){
        hex=padStringWithZero(new BigInteger(hex, 16).toString(2), 48);
        int [] res = new int [48];
        
        for(int i=0; i<hex.length();i++)
            res[i]=Character.getNumericValue(hex.charAt(i));
        
        return res;
    }
    
}
