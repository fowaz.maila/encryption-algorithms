/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.additivecipherbruteforce;

import java.math.BigInteger;
import java.util.Arrays;

/**
 *
 * @author Fowaz PC
 */
public class AES128 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        String key="5468617473206D79204B756E67204675";
        
        String [] [] keys = getWords(key);
        
        for(int i=0; i<keys.length; i++){
            for(int j=0; j<keys[i].length; j++)
                System.out.print(keys[i][j]+ " ");
            
            System.out.println("");
        }
        
        String [] [] res = cipherOneBlock("54776F204F6E65204E696E652054776F", key);
        
        for(int i=0; i<res.length; i++){
            for(int j=0; j<res[i].length; j++)
                System.out.print(res[i][j]+ " ");
            
            System.out.println("");
        }
            
        
    }
    
    public static String [] [] cipherOneBlock(String txt, String key){
        String [] [] words=getWords(key);
        String [] [] state = new String [4][4];
        
        int temp=0;
        for(int j=0; j<4; j++)
            for(int i=0; i<4; i++)
                state[i][j]=""+txt.charAt(temp++)+txt.charAt(temp++);
        
        //Round 0
        String [] [] roundKey = convertWordsToState(words[0][0], words[0][1], words[0][2], words[0][3]);
        state=addRoundKey(state, roundKey);
        
        for(int i=1; i<=9; i++) {
            //for each round 1..9
            state=subBytes(state);
            state=shiftRows(state);
            state=mixColumns(state);
            roundKey = convertWordsToState(words[i][0], words[i][1], words[i][2], words[i][3]);
            state=addRoundKey(state, roundKey);
        }
        
        //round 10 manually
        state=subBytes(state);
        state=shiftRows(state);
        roundKey = convertWordsToState(words[10][0], words[10][1], words[10][2], words[10][3]);
        state=addRoundKey(state, roundKey);
            
        return state;
    }
    
    public static String [] [] getInitialWords(String key) {
        //res is in hexadecimal
        String [] [] res = {{"", "", "", ""}};
        
        String hex = new BigInteger(key,16).toString(16);
        
        for(int i=0;i<8;i++){
            res[0][0]+=hex.charAt(i);
            res[0][1]+=hex.charAt(i+8);
            res[0][2]+=hex.charAt(i+16);
            res[0][3]+=hex.charAt(i+24);
        }
        
        return res;
    }
    
    public static String rotWord (String s) {
        
        String res="";
        
        for(int i=2; i<s.length();i++)
            res+=s.charAt(i);
        
        res+=s.charAt(0);
        res+=s.charAt(1);
        
        return res;
    }
      
    public static String subWord(char c1, char c2) {
        
        c1 = Character.toLowerCase(c1);
        c2 = Character.toLowerCase(c2);
        
        int i1,i2;
          
        String [] [] arr = {
            {"63", "7c", "77", "7b", "f2", "6b", "6f", "e5", "30", "01", "67", "2b", "fe", "d7", "ab", "76"},
            {"ca", "82", "c9", "7d", "fa", "59", "47", "f0", "ad", "d4", "a2", "af", "9c", "a4", "72", "c0"},
            {"b7", "fd", "93", "26", "36", "3f", "f7", "cc", "34", "a5", "e5", "f1", "71", "d8", "31", "15"},
            {"04", "c7", "23", "c3", "18", "96", "05", "9a", "07", "12", "80", "e2", "eb", "27", "b2", "75"},
            {"09", "83", "2c", "1a", "1b", "6e", "5a", "a0", "52", "3b", "d6", "b3", "29", "e3", "2f", "84"},
            {"53", "d1", "00", "ed", "20", "fc", "b1", "5b", "6a", "cb", "be", "39", "4a", "4c", "58", "cf"},
            {"d0", "ef", "aa", "fb", "43", "4d", "33", "85", "45", "f9", "02", "7f", "50", "3c", "9f", "a8"},
            {"51", "a3", "40", "8f", "92", "9d", "38", "f5", "bc", "b6", "da", "21", "10", "ff", "f3", "d2"},
            {"cd", "0c", "13", "ec", "5f", "97", "44", "17", "c4", "a7", "7e", "3d", "64", "5d", "19", "73"},
            {"60", "81", "4f", "dc", "22", "2a", "90", "88", "46", "ee", "b8", "14", "de", "5e", "0b", "db"},
            {"e0", "32", "3a", "0a", "49", "06", "24", "5c", "c2", "d3", "ac", "62", "91", "95", "e4", "79"},
            {"e7", "c8", "37", "6d", "8d", "d5", "4e", "a9", "6c", "56", "f4", "ea", "65", "7a", "ae", "08"},
            {"ba", "78", "25", "2e", "1c", "a6", "b4", "c6", "e8", "dd", "74", "1f", "4b", "bd", "8b", "8a"},
            {"70", "3e", "b5", "66", "48", "03", "f6", "0e", "61", "35", "57", "b9", "86", "c1", "1d", "9e"},
            {"e1", "f8", "98", "11", "69", "d9", "8e", "94", "9b", "1e", "87", "e9", "ce", "55", "28", "df"},
            {"8c", "a1", "89", "0d", "bf", "e6", "42", "68", "41", "99", "2d", "0f", "b0", "54", "bb", "16"}};
        
            switch(c1){
                case '0' : i1=0; break;
                case '1' : i1=1; break;
                case '2' : i1=2; break;
                case '3' : i1=3; break;
                case '4' : i1=4; break;
                case '5' : i1=5; break;
                case '6' : i1=6; break;
                case '7' : i1=7; break;
                case '8' : i1=8; break;
                case '9' : i1=9; break;
                case 'a' : i1=10; break;
                case 'b' : i1=11; break;
                case 'c' : i1=12; break;
                case 'd' : i1=13; break;
                case 'e' : i1=14; break;
                case 'f' : i1=15; break;
                default: i1=-1;
            }
            
            switch(c2){
                case '0' : i2=0; break;
                case '1' : i2=1; break;
                case '2' : i2=2; break;
                case '3' : i2=3; break;
                case '4' : i2=4; break;
                case '5' : i2=5; break;
                case '6' : i2=6; break;
                case '7' : i2=7; break;
                case '8' : i2=8; break;
                case '9' : i2=9; break;
                case 'a' : i2=10; break;
                case 'b' : i2=11; break;
                case 'c' : i2=12; break;
                case 'd' : i2=13; break;
                case 'e' : i2=14; break;
                case 'f' : i2=15; break;
                default: i2=-1;
            }
          
        return arr[i1][i2];
    }
    
    public static String wordsMultiplesOf4(String s, int index){
        BigInteger RC1 = new BigInteger("01000000", 16);
        BigInteger RC2 = new BigInteger("02000000", 16);
        BigInteger RC3 = new BigInteger("04000000", 16);
        BigInteger RC4 = new BigInteger("08000000", 16);
        BigInteger RC5 = new BigInteger("10000000", 16);
        BigInteger RC6 = new BigInteger("20000000", 16);
        BigInteger RC7 = new BigInteger("40000000", 16);
        BigInteger RC8 = new BigInteger("80000000", 16);
        BigInteger RC9 = new BigInteger("1B000000", 16);
        BigInteger RC10 = new BigInteger("36000000", 16);
        
        BigInteger [] RCs = {RC1, RC2, RC3, RC4, RC5, RC6, RC7, RC8, RC9, RC10}; 
        
        String res ="";
        String temp = rotWord(s);
        
        for(int i=0; i<s.length()-1; i+=2)
            res+=subWord(temp.charAt(i), temp.charAt(i+1));
        
        res= new BigInteger(res, 16).xor(RCs[index-1]).toString(16);
        
        return res;
    }
    
    public static String [] [] getWords(String key) {
        String [] [] keys = new String [11][4];
        
        for(int i=0; i<11; i++)
            for(int j=0; j<4; j++)
                keys[i][j]="";
        
        keys[0]=getInitialWords(key)[0];
        
        for(int i=1; i<11; i++){
            for(int j=0; j<4; j++){
                BigInteger wi4 = new BigInteger(keys[i-1][j], 16);
                if((4*i+j)%4!=0){
                    BigInteger wi1 = new BigInteger(keys[i][j-1], 16);
                    keys[i][j]= padStringWithZero(wi1.xor(wi4).toString(16), 8);
                } else {
                    BigInteger t = new BigInteger(wordsMultiplesOf4(keys[i-1][3], (4*i+j)/4), 16);
                    keys[i][j]= padStringWithZero(t.xor(wi4).toString(16), 8);
                }
            }
        }
        
        return keys;
    }
    
    public static String [] [] shiftRows(String [] [] arr) {
        String [] [] res = new String [arr.length] [arr[0].length];
        
        res[0]=arr[0];
        res[1]=new String [] {arr[1][1], arr[1][2], arr[1][3], arr[1][0]};
        res[2]=new String [] {arr[2][2], arr[2][3], arr[2][0], arr[2][1]};
        res[3]=new String [] {arr[3][3], arr[3][0], arr[3][1], arr[3][2]};
        
        return res;
    }
    
    public static String [] [] subBytes(String [] [] arr) {
        String [] [] res = new String [arr.length][arr[0].length];
        for(int i=0; i<arr.length; i++)
            for(int j=0; j<arr[0].length; j++)
                res[i][j]=subWord(arr[i][j].charAt(0), arr[i][j].charAt(1));
        return res;
    }
    
    
    public static String [] [] addRoundKey(String [] [] s, String [] [] k){
        String [] [] res = new String [s.length][s[0].length];
        for(int i=0; i<s.length; i++)
            for(int j=0; j<s[i].length; j++)
                res[i][j]=padStringWithZero(new BigInteger(s[i][j], 16).xor(new BigInteger(k[i][j],16)).toString(16),2);
        return res;
    }
    
    public static String [] [] mixColumns(String [] [] s) {
        String [] [] res = new String [s.length][s[0].length];
        
        String [] [] con = {{"02", "03", "01", "01"},
                            {"01", "02", "03", "01"},
                            {"01", "01", "02", "03"},
                            {"03", "01", "01", "02"}};
        
        for(int i=0; i<res.length; i++)
            for(int j=0; j<res[i].length; j++){
                
                String s0=multiplyGf2(con[i][0], s[0][j]);
                String s1=multiplyGf2(con[i][1], s[1][j]);
                String s2=multiplyGf2(con[i][2], s[2][j]);
                String s3=multiplyGf2(con[i][3], s[3][j]);
                
                BigInteger b0 = new BigInteger(s0, 16);
                BigInteger b1 = new BigInteger(s1, 16);
                BigInteger b2 = new BigInteger(s2, 16);
                BigInteger b3 = new BigInteger(s3, 16);
                
                BigInteger r = b0.xor(b1).xor(b2).xor(b3);
                res[i][j]= padStringWithZero(r.toString(16), 2);
                    
            }
        
        return res;
    }
    
    
    
    
    
    
    
    
    
    
    public static String padStringWithZero(String s, int length){
        while(s.length()!=length)
            s="0"+s;
        return s;
    }
    
    public static String multiplyGf2(String n1, String n2){
        BigInteger b;
        if(n1.equalsIgnoreCase("03") && !n2.equalsIgnoreCase("03")){
            b = new BigInteger("02", 16).multiply(new BigInteger(n2,16)).xor(new BigInteger(n2,16));
        } else if (n2.equalsIgnoreCase("03") && !n1.equalsIgnoreCase("03")){
            b = new BigInteger("02", 16).multiply(new BigInteger(n1,16)).xor(new BigInteger(n1,16));
        } else if (n2.equalsIgnoreCase("03") && n1.equalsIgnoreCase("03")){
            b = new BigInteger("02", 16).multiply(new BigInteger(n1,16)).xor(new BigInteger(n1,16));
        } else {
            b = new BigInteger(n1, 16).multiply(new BigInteger(n2,16));
        }
        
        if(b.toString(2).length()>8){
            b=new BigInteger(b.toString(2).substring(1),2).xor(new BigInteger("00011011", 2));
        }
        
        return padStringWithZero(b.toString(16), 2);
    }
    
    public static String [] [] convertWordsToState(String s1, String s2, String s3, String s4){
        String [] [] res = new String [4] [4];
        
        int temp=0;
        for(int i=0;i<4; i++)
            res[i][0]=""+s1.charAt(temp++)+s1.charAt(temp++);
        
        temp=0;
        for(int i=0;i<4; i++)
            res[i][1]=""+s2.charAt(temp++)+s2.charAt(temp++);
        
        temp=0;
        for(int i=0;i<4; i++)
            res[i][2]=""+s3.charAt(temp++)+s3.charAt(temp++);
        
        temp=0;
        for(int i=0;i<4; i++)
            res[i][3]=""+s4.charAt(temp++)+s4.charAt(temp++);
        return res;
    }
}
