/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.additivecipherbruteforce;

import java.util.Arrays;

/**
 *
 * @author Fowaz PC
 */
public class RC4Cipher {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        String v2= "10100010000101110001011000111010101010101010000001";
        System.out.println(changePoint(v2));
          
    }
    
    public static String cipher(String plain, String key, int n){
        
        int [] S = getS(n);
        int [] T = getT(key, n);
        S=initialPermutution(S, T, n);
        
        int [] k = generateKeyStream(plain, S, T, n);
        
        int [] p = new int [plain.length()];
        for(int i=0; i<plain.length(); i++)
            p[i]=Character.getNumericValue(plain.charAt(i));
        
        String res="";
        for(int i=0; i<plain.length(); i++)
            res+=p[i]^k[i];
        
        return res;
    }
    
    public static int [] generateKeyStream(String plain, int [] S, int [] T, int n) {
        int i=0, j=0, counter=0, temp;
        
        int [] k = new int[plain.length()];
        
        while(counter<plain.length()) {
            i=(i+1)%n;
            j=(j+S[i])%n;
            temp= S[i];
            S[i]=S[j];
            S[j]=temp;
            k[counter++]=S[(S[i]+S[j])%n];
        }
        
        return k;
    }
    
    public static int [] initialPermutution(int [] S, int [] T, int n) {
        int j=0, temp;
        for(int i=0; i<n; i++) {
            j=(j+S[i]+T[i])%n;
            temp= S[i];
            S[i]=S[j];
            S[j]=temp;
        }
        
        return S;
    }
    
    public static int [] getT(String key, int n){
        int [] T = new int[n];
        for(int i=0; i<n; i++)
            T[i]=Character.getNumericValue(key.charAt(i%key.length()));
        
        return T;
    }
    
    public static int [] getS(int n) {
        int [] S = new int [n];
        
        for(int i=0; i<n; i++)
            S[i]=i;
        
        return S;
    }
    
    public static int changePoint(String s) {
        
        //s=Integer.toBinaryString(Integer.parseInt(s));
        int n = s.length();
        int sn=countOnes(s, n-1);
        int t=0, st, temp;
        int m=Integer.MIN_VALUE;
        
        for(int i=1; i<=s.length(); i++) {
            st=countOnes(s, i-1);
            temp = Math.abs(n*st-i*sn);
            if(temp>m){
                m=temp;
                t=i;
            }
        }
        
        return t;
        
    }
    
    public static int countOnes(String s, int index) {
        int count=0;
        for(int i=0; i<=index ;i ++)
            if(s.charAt(i)=='1')
                count++;
        return count;
    }
    
    public static double firstDervative(String s) {
        String res="";
        
        for(int i=0; i<s.length()-1; i++){
            int i1 = Character.getNumericValue(s.charAt(i));
            int i2 = Character.getNumericValue(s.charAt(i+1));
            res+=i1^i2;
        }
        int n1 = countOnes(res, res.length()-1);
        double p = (double)n1/res.length();
        return p;
    }
    
    public static double secondDervative(String s) {
        String res="";
        
        for(int i=0; i<s.length()-1; i++){
//            int i1 = Integer.parseInt(Character.toString(s.charAt(i)));
//            int i2 = Integer.parseInt(Character.toString(s.charAt(i+1)));
            int i1 = Character.getNumericValue(s.charAt(i));
            int i2 = Character.getNumericValue(s.charAt(i+1));
            res+=i1^i2;
        }
        
        s=res;
        res="";
        
        for(int i=0; i<s.length()-1; i++){
//            int i1 = Integer.parseInt(Character.toString(s.charAt(i)));
//            int i2 = Integer.parseInt(Character.toString(s.charAt(i+1)));
            int i1 = Character.getNumericValue(s.charAt(i));
            int i2 = Character.getNumericValue(s.charAt(i+1));
            res+=i1^i2;
        }
        
        int n1 = countOnes(res, res.length()-1);
        double p = (double)n1/res.length();
        
        return p;
    }
    
}
