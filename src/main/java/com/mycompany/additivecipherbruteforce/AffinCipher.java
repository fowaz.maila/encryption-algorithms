/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.additivecipherbruteforce;


/**
 *
 * @author Fowaz PC
 */
public class AffinCipher {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
//        String s ="besttimeoftheyearisspringwhenflowersbloom";
//        String cipher = cipher(s, 11, 4);
//        System.out.println(cipher);
//        String plain = deCipher(cipher, calcOpposit(26, 11), 4);
//        System.out.println(plain);
        
        int [] arr = guessKeys("et", "WC");
        System.out.println(arr[0]+ " " +arr[1]);
          

    }
    
    public static String cipher(String s, int k1, int k2) {
        
        if(gcd(k1,26)!=1)
            return "Can't Cipher! key must be prime with 26";
        
        char c;
        int index;
        String res = "";
        String alpha = "abcdefghijklmnopqrstuvwxyz";
        
        for(int i=0;i<s.length();i++) {
            index=alpha.indexOf(s.charAt(i));
            index=Math.floorMod((index*k1)+k2, 26);
            res+=alpha.charAt(index);
        }
        
        return res.toUpperCase();
    }
    
    public static String deCipher(String s, int k1, int k2) {
        char c;
        int index;
        String res = "";
        String alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        
        for(int i=0;i<s.length();i++) {
            index=alpha.indexOf(s.charAt(i));
            index=Math.floorMod((index-k2)*k1,26);
            res+=alpha.charAt(index);
        }
        
        return res.toLowerCase();
    }
    
    public static int gcd (int x, int y) {
        if (x==1 || y==1)
            return 1;
        
        while (x!=y) {
            if(x>y) {
                x=x-y;
            } else {
                y=y-x;
            }
            
        }
        
        return x;
    }
    
    public static int calcOpposit(int n, int k) {
          
        
        int r1=n, r2=k, t1=0, t2=1, q, r, t;
        
        while(r2>0) {
            q=r1/r2;
            r=r1-q*r2;
            t=t1-q*t2;
            
            r1=r2; r2=r;
            t1=t2; t2=t;
        }
        
        if(r1==1)
            return Math.floorMod(t1, 26);
        else
            return Integer.MIN_VALUE;
    }
    
    
    public static int[][] invert(int a[][]) {
        int res [] [] = new int[2][2];
        
        int det = Math.floorMod(a[0][0]*a[1][1]-(a[0][1]*a[1][0]),26);
        int detInv = calcOpposit(26, det);
        
        int [] [] aa = {{a[1][1], -a[0][1]},{-a[1][0], a[0][0]}};
        res[0][0]=Math.floorMod(detInv*aa[0][0],26);
        res[0][1]=Math.floorMod(detInv*aa[0][1],26);
        res[1][0]=Math.floorMod(detInv*aa[1][0],26);
        res[1][1]=Math.floorMod(detInv*aa[1][1],26);
        return res;
    }
    
    public static int [] guessKeys(String s1, String s2) {
        
        String alpha = "abcdefghijklmnopqrstuvwxyz";
        int [] res = new int [2];
        s1=s1.toLowerCase();
        s2=s2.toLowerCase();
        
        int i1=alpha.indexOf(s1.charAt(0));
        int i2=alpha.indexOf(s1.charAt(1));
        int i3=alpha.indexOf(s2.charAt(0));
        int i4=alpha.indexOf(s2.charAt(1));
        int [] [] arr = {{i1, 1}, {i2, 1}};
        int[][] invert = invert(arr);
        
        res[0]=Math.floorMod(invert[0][0]*i3+invert[0][1]*i4,26);
        res[1]=Math.floorMod(invert[1][0]*i3+invert[1][1]*i4, 26);
        return res;
    }
    
}
